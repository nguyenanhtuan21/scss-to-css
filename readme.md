# Base project for using Laravel mix 

[Người thực hiện](https://github.com/NaTaShaRMN/) : 

[Live](https://natasharmn.github.io/scss-to-css/public/)

[PSD](https://github.com/NaTaShaRMN/scss-to-css/blob/master/resources/Home_3.psd)

## Goal

- Sử dụng Laravel mix nhưng không phải trong 1 Laravel project
- Cấu trúc cơ bản của 1 frontend project để làm quen với html template, sass,
browser sync.
- Tiếp tục sử dụng html, css, js

## Steps

- Bước 1: Cài đặt [nodejs](https://nodejs.org/en/) nếu chưa có
- Bước 3: Từ thư mục gốc chạy npm install.
- Bước 4: Chạy `npm run dev` sau đó vào thư mục public bằng trình duyệt. Tiếp đó thử chạy `npm run prod`, `npm run watch` xem khác nhau như thế nào.

## Trainee

- Tên : Nguyễn Anh Tuấn 
- Ngày thực hiện : 29/8/2019
- Link online : 
